#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
from uuid import uuid4
from tempfile import NamedTemporaryFile

from nose.tools import *
from zzutils import BaseTest

from ..base import ImportExportFormat

class YamlTest(BaseTest):

    ndocs = 10
    test_docs = map(lambda x: {'uuid':str(uuid4()), 'value': x}, range(ndocs))
    test_doc = test_docs[0]
    test_id = test_doc["uuid"]

    def setUp(self):

        self.ief = ImportExportFormat.new("yaml")
        self.f = NamedTemporaryFile()

    def tearDown(self):
        os.unlink(self.f.name)

    def test_write_docs(self):

        self.ief.write_docs(self.test_docs, self.f.file)
        self.f.file.flush()
        self.f.file.seek(0)
        dd = list(self.ief.doc_iter_from_stream(self.f.file))

        eq_(len(dd), self.ndocs)
        eq_(dd, self.test_docs)

    def test_doc_from_steram(self):

        self.ief.write_docs(self.test_docs, self.f.file)
        self.f.file.seek(0)
        doc = self.ief.doc_from_stream(self.f.file)
        doc_map = dict([ (d["uuid"], d) for d in self.test_docs ])
        eq_(doc, doc_map[doc["uuid"]])

# vi: ts=4 expandtab


