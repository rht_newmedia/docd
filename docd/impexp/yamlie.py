#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
import yaml

from .base import ImportExportFormat

@ImportExportFormat.register
class YamlFormat(ImportExportFormat):

    name = "yaml"

    def doc_iter_from_stream(self, stream):

        docs = yaml.load(stream)

        if not hasattr(docs, "next"):
            docs = [ docs, ]

        for d in docs:
            yield d

    def write_docs(self, doc_iter, stream):
        for doc in doc_iter:
            txt = yaml.dump(doc, default_flow_style=False)
            txt = txt.replace("!!python/unicode ", "")
            stream.write(txt + os.linesep)


