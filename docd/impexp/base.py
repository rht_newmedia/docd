#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os

class ImportExportFormat(object):

    IMPEXP_REGISTRY = {}

    @classmethod
    def register(cls, kls, name=""):

        name = name or getattr(kls, "name", None)

        if not name:
            raise KeyError("could not determine format name")

        if name in cls.IMPEXP_REGISTRY:
            raise KeyError("duplicate key in format register: %s" % name)

        cls.IMPEXP_REGISTRY[name] = kls
        return kls

    @classmethod
    def new(cls, s_type, *args, **kw):
        return cls.IMPEXP_REGISTRY[s_type](*args, **kw)

    def doc_from_stream(self, stream):
        for doc in self.doc_iter_from_stream(stream):
            return doc

    def doc_iter_from_steam(self, stream): raise NotImplementedError
    def write_docs(self, doc_iter, stream): raise NotImplementedError

