#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os

class DocumentStore(object):

    """Convenience class which binds a particular document type
       to a particular store
    """

    def __init__(self, doc_type, store):
        self.doc_type = doc_type
        self.store = store

    def put(self, doc):
        doc_id = self.doc_type.get_document_id(doc)
        if doc_id:
            return self.store.put(self.doc_type.name, doc_id, doc)

    def get(self, doc_id):
        return self.store.get(self.doc_type.name, doc_id)

    def list(self):
        return self.store.list(self.doc_type.name)

    def count(self):
        return self.store.count(self.doc_type.name)

    def delete(self, doc_id):
        return self.store.delete(self.doc_type.name, doc_id)

    def scan(self):
        for doc_id in self.store.list(self.doc_type.name):
            yield self.store.get(self.doc_type.name, doc_id)

    def query(self, name=""):

        qmap = self.doc_type.meta.get("queries", {})
        if not name:
            return qmap.keys()

        def doc_itr(itr):
            for hit in itr:
                yield hit["_source"]

        return doc_itr(self.store.query(self.doc_type.name, qmap[name]))

        #for doc_id in self.store.list(self.doc_type.name):
        #    yield self.store.get(self.doc_type.name, doc_id)

    def initialize(self):
        return self.store.initialize(self.doc_type.name, self.doc_type.meta)

    def bulk_insert(self, doc_iter):
        for doc in doc_iter:
            self.put(doc)


# vi: ts=4 expandtab


