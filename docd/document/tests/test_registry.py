#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os
from uuid import uuid4

from nose.tools import *
from zca import zca_manager as zca
from zzutils import BaseTest

from ..interfaces import IDocumentTypeRegistry
from ..registry import DirectoryDocumentTypeRegistry

class DocStoreTest(BaseTest):

    basedir = os.path.dirname(__file__)

    doc_type = "test_foo"

    #ndocs = 10
    #test_docs = map(lambda x: self._gen_doc(), range(ndocs))
    #test_doc = test_docs[0]
    #test_id = test_doc["uuid"]

    def _gen_doc(self):
        return {
            'foo_id': str(uuid4()),
            'bar': str(uuid4()),
            'biz': str(uuid4()),
        }

    def setUp(self):
        
        d = os.path.join(self.basedir, "doc_types")
        r = DirectoryDocumentTypeRegistry(d)
        zca.register_utility(r)

    def test_registry(self):

        r = zca.get_utility(IDocumentTypeRegistry)
        dt = r.new(self.doc_type)

        eq_(dt.meta["id_attr"], "foo_id")

    def test_id_attr(self):

        doc = self._gen_doc()
        r = zca.get_utility(IDocumentTypeRegistry)
        dt = r.new(self.doc_type)
        eq_(dt.get_document_id(doc), doc["foo_id"])

    def test_id_fmt(self):

        doc = self._gen_doc()
        r = zca.get_utility(IDocumentTypeRegistry)
        dt = r.new(self.doc_type + "_combo")

        x = "%s_%s" % (doc["bar"], doc["biz"])
        eq_(dt.get_document_id(doc), x)

# vi: ts=4 expandtab


