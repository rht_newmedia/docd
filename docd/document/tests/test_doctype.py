#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

from tempfile import mkdtemp
from shutil import rmtree
from random import choice

from nose.tools import *
from zzutils import BaseTest

from ..doctype import DocumentType
from ..docstore import DocumentStore
from ...store import FilesystemStore

from uuid import uuid4

class DocStoreTest(BaseTest):

    doc_type = "testdoc"

    ndocs = 10
    test_docs = map(lambda x: {'uuid':str(uuid4()), 'value': x}, range(ndocs))
    test_doc = test_docs[0]
    test_id = test_doc["uuid"]

    def setUp(self):
        
        self.basedir = mkdtemp()
        self.store = FilesystemStore(self.basedir)
        self.dt = DocumentType(self.doc_type)
        self.doc_store = DocumentStore(self.dt, self.store)

    def tearDown(self):

        rmtree(self.basedir)

    def test_empty_list(self):

        eq_(self.doc_store.list(), [])

    def test_put(self):

        self.doc_store.put(self.test_doc)
        oo = self.doc_store.list()
        eq_(len(oo), 1)
        eq_(oo[0], self.test_id)

    def test_get(self):

        self.doc_store.put(self.test_doc)
        d = self.doc_store.get(self.test_id)
        eq_(d, self.test_doc)

    def test_count(self):

        self.doc_store.put(self.test_doc)
        eq_(self.doc_store.count(), 1)

    def test_initialize(self):
        self.doc_store.initialize()

    def test_delete(self):

        self.doc_store.put(self.test_doc)
        eq_(self.doc_store.list(),  [self.test_id,])
        self.doc_store.delete(self.test_id)
        eq_(self.doc_store.list(), [])

    def test_bulk_insert(self):

        self.doc_store.bulk_insert(self.test_docs)
        eq_(len(self.doc_store.list()),  self.ndocs)

    def test_scan(self):

        self.doc_store.bulk_insert(self.test_docs)
        d_map = dict([ (d["uuid"], d) for d in self.doc_store.scan() ])

        eq_(len(d_map), self.ndocs)
        d = choice(self.test_docs)
        eq_(d_map[d["uuid"]], d)

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)
    import sys

# vi: ts=4 expandtab


