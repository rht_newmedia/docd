#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from zope.interface import Interface, Attribute

class IDocumentType(Interface):

    """ schema and id identification
    """

    name = Attribute("store name")
    schema = Attribute("json schema for the document")

    # public?
    meta = Attribute("auxillary info, often storage specific")

    def get_document_id(document):

        """ return id for document, probably from document contents """

class IDocumentTypeRegistry(Interface):

    def new(document_type_name, create=False):

        """ document type factory """

class IDocumentStore(Interface):

    """ Associate a given document type to a storage location
    """

    doc_type = Attribute("managed doucment type")
    store = Attribute("associated storage")

    def put(doc):

        """ store document """

    def get(doc_id):

        """ return document instance """

    def list():

        """ return document ids """

    def delete(doc_id):

        """ delte document """

    def scan():

        """ iterate over all documents """

    def bulk_insert(doc_iter):

        """ store all documents """

# vi: ts=4 expandtab


