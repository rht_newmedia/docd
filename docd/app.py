#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
logging.basicConfig(level=logging.INFO)

import os

from zzapp import BaseApplication
from zca import zca_manager as zca

from .store import load_store_configuration
from .store.interfaces import IStore

from .esearch import register_clusters
from .esearch import load_esearch_utilities
from .esstore import load_esstore_utilities

from .document import DirectoryDocumentTypeRegistry
from .document import DocumentType, DocumentStore

class Application(BaseApplication):

    app_name = "docd"

    def init_post_params(self):

        d = os.path.join(os.environ["HOME"], ".docd", "doc_types")
        zca.register_utility(DirectoryDocumentTypeRegistry(d))

        INFO("post params init")
        GP = self.params.get_parameter

        es_cfg = GP("elasticsearch.clusters")
        if es_cfg:
            load_esearch_utilities(es_cfg)

        dir_cfg = GP("esstore.document_index_route")
        load_esstore_utilities(dir_cfg)

        s_cfg = self.params.get("storage")
        if s_cfg:
            load_store_configuration(s_cfg)

# vi: ts=4 expandtab


