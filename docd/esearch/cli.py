#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
logging.basicConfig(level=logging.INFO)

import os
import click

from zca import zca_manager as zca
from zzutils import as_yaml

from ..impexp import ImportExportFormat

from .interfaces import ICluster
from .interfaces import IIndexRouter

@click.group()
@click.pass_context
def esearch(ctx):
    pass

@esearch.group()
@click.option("-n", "--name", help="cluster name", default="default")
@click.pass_context
def cluster(ctx, name):
    ctx.cluster = zca.get_utility(ICluster, name)

@cluster.command()
@click.pass_context
def info(ctx):

    c = ctx.parent.cluster
    click.echo(as_yaml(c.info))

@cluster.command()
@click.pass_context
def ls(ctx):

    def _extract_info(name, istats):
        return { 
            'name': name, 
            'ndocs': istats["primaries"]["docs"]["count"],
            'size': istats["primaries"]["store"]["size_in_bytes"]/1024,
        }

    c = ctx.parent.cluster
    imap = c.list_index().get("indices", {})
    for k in sorted(imap):
        i = _extract_info(k, imap[k])
        click.echo("%(name)-30s %(ndocs)10s %(size)20s" % i)

@cluster.command()
@click.pass_context
def ls_template(ctx):

    c = ctx.parent.cluster
    tmap = c.get_template("")
    click.echo(os.linesep.join(sorted(tmap.keys())))

@cluster.command()
@click.option("-f", "--format", default="yaml")
@click.option("-n", "--name", default="")
@click.argument("dest", type=click.File("w"), default="-")
@click.pass_context
def get_template(ctx, format, name, dest):

    c = ctx.parent.cluster
    f = ImportExportFormat.new(format)
    tmap = c.get_template(name)

    f.write_docs([tmap, ], dest)

@cluster.command()
@click.option("-f", "--format", default="yaml")
@click.option("-n", "--name")
@click.pass_context
def delete_template(ctx, format, name):
    c = ctx.parent.cluster
    c.delete_template(name)

@cluster.command()
@click.option("-f", "--format", default="yaml")
@click.argument("source", type=click.File(), default="-")
@click.pass_context
def put_template(ctx, format, source):

    c = ctx.parent.cluster
    f = ImportExportFormat.new(format)
    body = f.doc_from_stream(source)

    for k,v in body.items():
        c.put_template(k, v)

@esearch.group()
@click.option("-n", "--name", help="index name")
@click.option("-c", "--cluster_name", help="cluster name")
@click.pass_context
def index(ctx, name, cluster_name):

    ir = zca.get_utility(IIndexRouter)
    ctx.index = ir.route(name, cluster_name)

@index.command()
@click.pass_context
def count(ctx):

    idx = ctx.parent.index
    click.echo(idx.n_documents)

@index.command()
@click.option("-f", "--format", default="json_line")
@click.argument("outfile", type=click.File("w"))
@click.pass_context
def scan(ctx, format, outfile):

    idx = ctx.parent.index
    f = ImportExportFormat.new(format)
    f.write_docs(idx.all_doc_iter(), outfile)

    #click.echo(idx.n_documents)

# vi: ts=4 expandtab
