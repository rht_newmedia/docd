#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

import os
from uuid import uuid4
from time import sleep
from nose.tools import *

from .base import BaseTestCase

from zzutils import as_yaml
from zca import zca_manager as zca

from ..index import Index
from ..interfaces import IIndexRouter
#from ..router import dflt_index_router

class TestESIndex(BaseTestCase):

    def setUp(self):

        super(TestESIndex, self).setUp()
        self._load_params()
        #register_esstores()

        self.router = zca.get_utility(IIndexRouter)
        self.idx = self.router.route(self.sample_index)

        self._load_dflt_doc()

    def test_es_index(self):
        ok_("foo" in str(self.idx))

    def test_monthly_index(self):
        idx = self.router.route(self.sample_index, style="monthly")

    @raises(KeyError)
    def test_route_es_dont_exist(self):
        i = self.router.route("test", "idontexist")

    def test_es_index_cluster(self):

        i = Index("foo", cluster=self.dflt_cluster)
        ok_("foo" in str(i))

    def test_es_ndocs(self):
        eq_(self.idx.n_documents, 1)

    def test_es_ndocs_noindex(self):

        i = self.router.route(self.sample_index + "idontexist")
        eq_(i.n_documents, 0)

    def test_es_stats(self):

        s = self.idx.get_stats(metric="docs")
        eq_(s["_all"]["primaries"]["docs"]["count"], 1)

    def test_es_stats_noindex(self):

        i = self.router.route(self.sample_index + "idontexist")
        s = i.get_stats(metric="docs")
        eq_(s, [])

    def test_delete_index(self):
        ok_(self.idx.exists)
        self.idx.delete_index()
        ok_(not self.idx.exists)

    def test_delete_index_noindex(self):
        i = self.router.route(self.sample_index + "idontexist")
        i.delete_index()
        ok_(not i.exists)

    def test_read_idx(self):
        self.idx.index_style = "monthly"
        ok_(self.idx.index_read_name.endswith("*"))

    def test_delete(self):
        self.idx.delete(self.sample_doc_type, self.sample_doc['uuid'])
        self.idx.refresh()
        eq_(self.idx.n_documents, 0)

    def test_get_doc(self):

        d = self.idx.get_doc(self.sample_doc_type, self.sample_doc['uuid'])
        eq_(d, self.sample_doc)

    def test_all_hit_iter(self):

        hh = list(self.idx.all_hit_iter())
        eq_(len(hh), 1)
        eq_(hh[0]["_source"]["foo"], "bar")

    def test_all_doc_iter(self):

        dd = list(self.idx.all_doc_iter())
        eq_(len(dd), 1)
        eq_(dd[0]["doc_type"], self.sample_doc_type)
        eq_(dd[0]["foo"], "bar")

    def test_index(self):

        doc_id = str(uuid4())
        doc = { 'uuid': doc_id, 'foo': "index_test", 'baz': 42, }

        r = self.idx.index(
            doc_type=self.sample_doc_type, 
            id=doc_id,
            body=doc
        )

        doc2 = self.idx.get_doc(self.sample_doc_type, doc_id)
        eq_(doc, doc2)

    def test_bulk(self):

        def bulk_iter(doc_iter):
            for doc in doc_iter:
                yield {
                    '_index': self.sample_index,
                    '_type': self.sample_doc_type,
                    '_id': doc["uuid"], 
                    '_source': doc, 
                }

        nsamples = 10
        samples = map(lambda x: self._gen_sample_doc(), range(nsamples))
        r = self.idx.bulk(bulk_iter(samples))
        eq_(r[0], nsamples)

        self.idx.refresh()
        d_map = self.idx.get_doc_map(self.sample_doc_type)
        eq_(len(d_map), nsamples+1)
            
    def test_search(self):
 
        r = self.idx.search(body=self.sample_filter)
        eq_(r["hits"]["total"], 1)
        eq_(r["hits"]["hits"][0]["_source"]["biz"], 10)
 
##     def test_scan(self):
## 
##         hh = list(self.idx.scan())
##         eq_(len(hh), 1)
##         eq_(hh[0]["_source"]["biz"], 10)
## 
## #    @raises(ValueError)
## #    def test_bad_cluster(self):
## #
## #        i = ESIndex("foo", cluster_name="idontexst")
## 

# vi: ts=4 expandtab
