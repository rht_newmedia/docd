#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

import os
from time import sleep
from uuid import uuid4

import unittest
from nose.tools import *

from elasticsearch.exceptions import NotFoundError

from .base import BaseTestCase

from zca import zca_manager as zca
from zzutils import as_yaml
from zzutils import read_yaml_file

from ..cluster import Cluster
from ..cluster import register_clusters

class TestCluster(BaseTestCase):

    template_name = "unittest_template"
    template_filename = os.path.join(BaseTestCase.test_dir, "template.yml")

    def setUp(self):
        self._load_params()

    def test_cmap_cfg(self):

        cmap = {
            'one': "http://one",
            'two': {
                'hosts': "http://one",
                'default': True,
            }
        }
        register_clusters(cmap)

    def test_duplicate_cmap_cfg(self):

        cmap = {
            'one': "http://one",
            'two': { 'hosts': "http://two", 'default': True, },
            'three': { 'hosts': "http://three", 'default': True, },
        }
        register_clusters(cmap)

    def test_nodefault_cmap_cfg(self):

        cmap = {
            'one': "http://one",
            'two': "http://two",
        }
        register_clusters(cmap)

    def test_str(self):

        ok_('default' in str(self.dflt_cluster))
        i = self.dflt_cluster.info

    def test_info(self):

        i = self.dflt_cluster.info
        ok_('cluster_name' in i)

    def test_settings(self):

        s = self.dflt_cluster.settings
        ok_('persistent' in s)
        ok_('transient' in s)

    def test_stats(self):

        s = self.dflt_cluster.stats
        ok_('indices' in s)
        ok_('nodes' in s)

    def test_health(self):

        s = self.dflt_cluster.health
        ok_('status' in s)

    def test_get_state(self):

        s = self.dflt_cluster.get_state(metric="version")
        ok_('version' in s)

    def test_get_bad_index_stats(self):

        s = self.dflt_cluster.get_index_stats(index="idontexist")
        eq_(s, {})

    def test_list_index(self):

        s = self.dflt_cluster.list_index()
        ok_('indices' in s)

    def test_get_segments(self):

        s = self.dflt_cluster.get_segments()
        ok_('indices' in s)

    def test_index(self):

        doc_id = self._load_dflt_doc()
        ok_(doc_id)

    def test_bulk(self):

        self.add_cleanup(self._cleanup_index)

        r = self.dflt_cluster.bulk(
            index=self.sample_index, 
            doc_type=self.sample_doc_type,
            actions=[self.sample_doc,],
        )
        eq_(r[0], 1)

    def test_scan(self):

        doc_id = self._load_dflt_doc()
        ss = list(self.dflt_cluster.scan(
            index=self.sample_index, 
            doc_type=self.sample_doc_type,
        ))
        eq_(len(ss), 1)
        eq_(ss[0]['_source']['biz'], 10)

    def test_scan_bad_index(self):

        doc_id = self._load_dflt_doc()
        ss = list(self.dflt_cluster.scan(
            index="idontexist",
        ))
        eq_(ss, [])

    def test_search(self):

        doc_id = self._load_dflt_doc()
        r = self.dflt_cluster.search(
            index=self.sample_index,
            doc_type=self.sample_doc_type,
            body=self.sample_filter
        )
        eq_(r["hits"]["total"], 1)
        eq_(r["hits"]["hits"][0]["_source"]["biz"], 10)

    def test_extract_source(self):

        self.add_cleanup(self._cleanup_index)

        r = self.dflt_cluster.index(
            index=self.sample_index, 
            doc_type=self.sample_doc_type,
            body=self.sample_doc,
            refresh=True,
        )
        ok_("_id" in r)

        ss = list(self.dflt_cluster.scan(
            index=self.sample_index, 
            doc_type=self.sample_doc_type,
        ))
        eq_(len(ss), 1)

        doc = self.dflt_cluster.extract_source(ss[0])
        eq_(doc['doc_type'], self.sample_doc_type)
        eq_(doc['biz'], 10)

    def test_delete_index(self):

        r = self.dflt_cluster.index(
            index=self.sample_index, 
            doc_type=self.sample_doc_type,
            body=self.sample_doc,
            refresh=True,
        )
        ok_("_id" in r)
        self.dflt_cluster.delete_index(self.sample_index)

    def test_delete_bad_index(self):

        self.dflt_cluster.delete_index(self.sample_index+"idontexist")

    def test_host_list(self):

        c = Cluster(name="test", hosts=['http://some_url'])
        ok_('some_url' in c.url)

    def _delete_template(self, name=template_name):
        if "unittest" in name:
            self.dflt_cluster.delete_template(name)

    def test_put_template(self):
        self.add_cleanup(self._delete_template)
        tmap = read_yaml_file(self.template_filename)
        for k,v in tmap.items():
            self.dflt_cluster.put_template(k, v)

        tmap = self.dflt_cluster.get_template("")
        ok_(self.template_name in tmap.keys())


