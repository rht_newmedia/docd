#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

import os
import string
from uuid import uuid4
from random import sample, randint

from zca import zca_manager as zca
from zzparameters import Parameters
from zzutils import BaseTest

from ..interfaces import ICluster
from .. import load_esearch_utilities
#from ..cluster import register_clusters

class BaseTestCase(BaseTest):

    test_dir = os.path.dirname(__file__)

    sample_index = "indexfoo_unittest_"
    sample_doc_type = "doctypefoo_unittest_"
    sample_doc = {
        'uuid': str(uuid4()),
        'foo': "bar",
        'biz': 10,
    }

    sample_filter = { 'query': {'filtered': {'filter': 
        {'term': { 'foo': "bar" }},
    }}}

    def _gen_sample_doc(self): 
        return {
            'uuid': str(uuid4()),
            'foo': sample(string.lowercase, 8),
            'biz': randint(1, 100),
        }

    def _cleanup_index(self, index_name=None):
        index_name = index_name or self.sample_index
        if 'unittest' not in index_name:
            raise ValueError("refuse delete non unittest index %s" % index_name)
        self.dflt_cluster.delete_index(index_name)
    
    def _load_params(self):

        self.params = Parameters()
        fname = os.path.join(self.test_dir, "config.yml")
        self.params.load_param_file(fname)

        c_cfg = self.params.get_parameter("elasticsearch.clusters")
        load_esearch_utilities(c_cfg)

        #register_clusters(self.params["elasticsearch"]["clusters"])
        self.dflt_cluster = zca.get_utility(ICluster, "default")

    def _load_dflt_doc(self, id=None):
        """ requires _load_parasm """

        self.add_cleanup(self._cleanup_index)

        r = self.dflt_cluster.index(
            index=self.sample_index, 
            doc_type=self.sample_doc_type,
            id=id or self.sample_doc["uuid"],
            body=self.sample_doc,
            refresh=True,
        )
        return r["_id"]
