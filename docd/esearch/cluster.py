#!/usr/bin/env python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from __future__ import absolute_import

import logging; log = logging.getLogger("esearch")
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

from elasticsearch import Elasticsearch
from elasticsearch import TransportError
from elasticsearch.exceptions import NotFoundError
from elasticsearch.exceptions import ConnectionError
import elasticsearch.helpers

from zca import zca_manager as zca

from .interfaces import ICluster

def register_clusters(cluster_map):

    """register_clusters:
            configure named clusters, probably from a config
            file.  
    """

    default = None

    for name, kw in cluster_map.items():

        if name == "default":
            default = "default"
        
        # if config provides a string, assume it's a host url.
        if isinstance(kw, basestring):
            kw = { 'hosts': kw }

        if 'default' in kw:
            if not default:
                default = name
                zca.register_utility(Cluster(name, **kw), name="default")
            else:
                WARN("ignoring duplicate default %s" % name)

        # otherwise, kw should be a map of Elasticsearch() arguments
        zca.register_utility(Cluster(name, **kw), name=name)

    if not default:
        INFO("adding imlicit default cluster")
        c = Cluster("default", hosts="http://localhost")
        zca.register_utility(c, name=name)

@zca.implementer(ICluster)
class Cluster(object):

    """Cluster: proxy to an Elasticsearch client
    """

    #index_metrics = """
    #    docs store indexing get search completion
    #    fielddata flush merge request_cache refresh
    #    suggest warmer translog
    #""".split()

    def __init__(self, name, **es_kw):

        self.name = name

        assert 'hosts' in es_kw
        self.es_kw = es_kw

        self._es = None

    def __str__(self):
        return "<Cluster %s %s>" % (self.name, self.url)

    @property
    def url(self):
        h = self.es_kw["hosts"]
        if isinstance(h, basestring):
            return h
        return h[0]

    @property
    def es(self):

        if not self._es:

            INFO("openning es connection %s" % self.name)
            DEBUG("cluster params %s" % self.es_kw)
            self._es = Elasticsearch(**self.es_kw)

        return self._es

    @property
    def info(self): 
        return self.es.info()
    
    @property
    def settings(self): 
        return self.es.cluster.get_settings()

    @property
    def stats(self): 
        return self.es.cluster.stats()

    @property
    def health(self): 
        return self.es.cluster.health()

    def get_state(self, metric=""): 
        return self.es.cluster.state(metric=metric)

    def get_index_stats(self, index='_all', metric=""): 
        try:
            return self.es.indices.stats(index=index, metric=metric)
        except NotFoundError, e:
            return {}

    def list_index(self, index="_all"):
        return self.get_index_stats(index, metric="docs,store,fielddata")

    def get_segments(self): 
        return self.es.indices.segments("_all")

    def _not_found_wrapper(self, an_iter):
        try:
            for i in an_iter:
                yield i
        except NotFoundError, e:
            return

#    def probe_indices(self):
#        res = self.es.indices.status()

    def index(self, **kw):
        return self.es.index(**kw)

    def search(self, **kw):
        return self.es.search(**kw)

    def bulk(self, actions, **kw):

        B = elasticsearch.helpers.bulk
        return B(self.es, actions=actions, **kw)

    def scan(self, **kw):

        S = elasticsearch.helpers.scan
        return self._not_found_wrapper(S(self.es, **kw))

#    def get_all_iter(self, index_name, doc_type, **kw):
#        
#        S = elasticsearch.helpers.scan
#        es_iter = S(self.es, index=index_name, doc_type=doc_type, **kw)
#        return self._not_found_wrapper(es_iter)

    def delete_index(self, index_name):
        try:
            self.es.indices.delete(index_name)
            return True
        except NotFoundError, e:
            return False

    @staticmethod
    def extract_source(hit, extract_metadata=True, id_attr="id"):

        d = hit["_source"]

        if extract_metadata:
            d[id_attr] = hit["_id"]
            if 'doc_type' not in d:
                d["doc_type"] = hit["_type"]

        return d

    def get_template(self, name):
        return self.es.indices.get_template(name)

    def delete_template(self, name):
        try:
            return self.es.indices.delete_template(name)
        except NotFoundError, e:
            return

    def put_template(self, name, body):
        return self.es.indices.put_template(name, body)
        
# vi: ts=4 expandtab


