#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from zope.interface import Interface, Attribute

class ICluster(Interface):

    """ICluster: proxy to an Elasticsearch cluster

        provides:

            - named connections provided by configuration.

            - simple convenience wrappers to selected commands.

    """

    name = Attribute("cluster name")
    es = Attribute("Elasticsearch client")

    info = Attribute("info from ES cluster")
    stats = Attribute("index stats from ES cluster")

    # ...

class IIndex(Interface):

    """ IIndex: manage index operations """

class IIndexRouter(Interface):

    def route(index_name, cluster_name=None):

        """ route an index name (and possibly cluster_name) to
            an index

            index_name may contain an "@" to override conigured 
            routing, i.e, "index@cluster".  

            return an Index.
        """

# vi: ts=4 expandtab


