
from zca import zca_manager as zca

from .cluster import Cluster
from .cluster import register_clusters

from .router import IndexRouter
from .router import dflt_index_router

def load_esearch_utilities(cluster_config, index_router=None):

    register_clusters(cluster_config)
    zca.register_utility(index_router or dflt_index_router)

