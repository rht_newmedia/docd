#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from __future__ import absolute_import

import logging; log = logging.getLogger("esearch")
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

from zope.interface.interfaces import ComponentLookupError

from zca import zca_manager as zca

from .interfaces import ICluster
from .interfaces import IIndexRouter

from .index import Index

@zca.implementer(IIndexRouter)
class IndexRouter(object):

    def __init__(self, 
        dflt_cluster="default", 
        index_registry=None,
        cluster_map=None
    ):

        self.dflt_cluster = dflt_cluster
        self.cluster_map = cluster_map or {}
        self.index_registry = index_registry or {}

    def route(self, index_name, cluster_name=None, **kw):

        """route: given an index_name and possibly a cluster_name, 
           return an index
        """

        assert not (cluster_name and "@" in index_name)
        if not cluster_name:
            index_name, _, cluster_name = index_name.partition("@")
            cluster_name = cluster_name or "default"

        try:
            c = zca.get_utility(ICluster, cluster_name)
        except ComponentLookupError, e:
            raise KeyError("could not resolve cluster %s" % cluster_name)

        cls = self.index_registry.get(index_name, Index)
        return cls(index_name, c, **kw)

dflt_index_router = IndexRouter()

# vi: ts=4 expandtab


