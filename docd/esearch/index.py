#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from __future__ import absolute_import

import logging; log = logging.getLogger("esearch")
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

from uuid import uuid4

from zca import zca_manager as zca

from .interfaces import ICluster
from .interfaces import IIndex

#from .interfaces import IESIndexRouter
#from .interfaces import ISearchFilter
#from .interfaces import IHitTransform
#from .interfaces import IDocumentTransform

from .cluster import Cluster
from .search import Query
from elasticsearch.exceptions import NotFoundError

@zca.implementer(IIndex)
class Index(object):

    """Index: Manage Index operations
    """

    def __init__(self, name, cluster, style=None):

        self.name = name
        self.index_name_base = self.name
        self.index_style = style
        self.cluster = cluster

    def __str__(self):
        return "<Index %s [%s]>" % (self.name, self.cluster.name)

    @property
    def index_read_name(self):
        if self.index_style:
            return self.index_name_base + "*"
        return self.index_name_base

    @property
    def index_write_name(self):
        return self.index_name_base

    @property
    def exists(self):
        return self.cluster.es.indices.exists(self.index_read_name)

    @property
    def n_documents(self):
        try:
            S = self.cluster.es.indices.stats
            s = S(self.index_read_name, metric="docs")
            return s["_all"]["primaries"]["docs"]["count"]
        except NotFoundError, e:
            return 0

    def get_doc_map(self, doc_type):
        """ possibly very expensive! """

        rr = self.all_hit_iter(doc_type)
        return dict([ (r["_id"], r["_source"]) for r in  rr ])

    def get_stats(self, metric=""):
        try:
            S = self.cluster.es.indices.stats
            return S(self.index_read_name, metric=metric)
        except NotFoundError, e:
            return []

    def delete_index(self):
        try:
            return self.cluster.es.indices.delete(self.index_read_name)
        except NotFoundError, e:
            pass

    def refresh(self):
        self.cluster.es.indices.refresh(self.index_write_name)

    def get(self, doc_type, doc_id):

        res = self.cluster.es.get(
            index=self.index_read_name, 
            doc_type=doc_type, 
            id=doc_id
        )
        return res

    def get_doc(self, doc_type, doc_id):

        d = self.get(doc_type, doc_id)
        if d:
            return d["_source"]

    def delete(self, doc_type, doc_id):

        res = self.cluster.es.delete(
            index=self.index_read_name, 
            doc_type=doc_type, 
            id=doc_id
        )
        return res

    def scan(self, **kw):

        res = self.cluster.scan(index=self.index_read_name, **kw)
        return res

    def query(self, doc_type, qdef):
        q = Query(qdef)
        return self.scan(doc_type=doc_type, **q.get_scan_kw())

    def all_hit_iter(self, doc_type=None):
        return self.scan(doc_type=doc_type)

    def all_doc_iter(self, doc_type=None, extract_metadata=True):

        F = Cluster.extract_source
        def doc_iter(h_iter):
            for h in h_iter:
                yield F(h, extract_metadata=extract_metadata)
        
        return doc_iter(self.all_hit_iter(doc_type))

    def index(self, **kw):

        res = self.cluster.es.index(index=self.index_write_name, **kw)
        return res

    def search(self, **kw):

        res = self.cluster.search(index=self.index_read_name, **kw)
        return res

    def bulk(self, actions, **kw):
        return self.cluster.bulk(actions=actions, **kw)

#    def _scan_iter(self, filters=None, hit_transforms=None, 
#            as_doc=False, doc_transforms=None, **kw):
#
#        filters = filters or []
#        hit_transforms = hit_transforms or []
#        doc_transforms = doc_transforms or []
#
#        assert not (doc_transforms and not ad_doc)
#
#        if not (filters or hit_transforms or as_doc):
#            return self.scan(**kw)
#
#        def _gen_filter(fname_list):
#            for fname in fname_list:
#                f = zca.get_utility(ISearchFilter, fname)
#                yield f.filter()
#
#        def _gen_transforms(hit_tname_list, as_doc, doc_tname_list):
#
#            for tname in hit_tname_list:
#                yield zca.get_utility(IHitTransform, tname).transform_hit
#
#            if as_doc:
#                t = zca.get_utility(IHitTransform, "extract_source")
#                yield t.transform_hit
#
#            for tname in doc_tname_list:
#                yield zca.get_utility(IDocumentTransform, tname).transform_doc
#
#        def t_iter(transforms, itr):
#            try:
#                for h in itr:
#                    for t in transforms:
#                        h = t(h)
#                    yield h
#            except NotFoundError, e:
#                return 
#
#        tt = list(_gen_transforms(hit_transforms, as_doc, doc_transforms))
#        q = {'query': {'filtered': {'filter': { 'bool': { 'must': 
#            list(_gen_filter(filters)),
#        }}}}}
#        return t_iter(tt, self.scan(query=q, **kw))

# vi: ts=4 expandtab
