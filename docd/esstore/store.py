#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

import os

from zca import zca_manager as zca

from ..store.interfaces import IStore
from ..store import BaseStore

from .interfaces import IDocumentIndexRouter

@zca.implementer(IStore)
@BaseStore.register_store
class ESStore(BaseStore):

    """Convenience class which binds a particular document type
       to a particular store
    """

    storage_type = "elasticsearch"

    def __init__(self, path):
        self.path = path

    def _get_index_for_doc_type(self, doc_type_name):

        di_router = zca.get_utility(IDocumentIndexRouter)
        return di_router.route(doc_type_name)

    def refresh(self, doc_type):
        idx = self._get_index_for_doc_type(doc_type)
        idx.refresh()

    def get(self, doc_type, doc_id):

        idx = self._get_index_for_doc_type(doc_type)
        return idx.get_doc(doc_type, doc_id)

    def put(self, doc_type, doc_id, doc):

        idx = self._get_index_for_doc_type(doc_type)
        return idx.index(doc_type=doc_type, id=doc_id, body=doc)

    def list(self, doc_type):

        idx = self._get_index_for_doc_type(doc_type)
        r_itr = idx.scan(doc_type=doc_type, _source=False)

        def _extract_id(itr):
            for d in itr:
                yield d["_id"]

        return _extract_id(r_itr)

    def count(self, doc_type):

        idx = self._get_index_for_doc_type(doc_type)
        r = idx.search(doc_type=doc_type, size=0)
        return r["hits"]["total"]

    def delete(self, doc_type, doc_id):

        idx = self._get_index_for_doc_type(doc_type)
        return idx.delete(doc_type, doc_id)

    def initialize(self, doc_type, meta):

        idx = self._get_index_for_doc_type(doc_type)

        # register any index templates
        es = meta.get("elasticsearch", {})
        tmap = es.get("index_templates", {})
        for name, tmpl in tmap.items():
            INFO("installing template %s" % name)
            idx.cluster.es.indices.put_template(name=name, body=tmpl)

    def query(self, doc_type, qdef):

        idx = self._get_index_for_doc_type(doc_type)
        return idx.query(doc_type, qdef)


# vi: ts=4 expandtab


