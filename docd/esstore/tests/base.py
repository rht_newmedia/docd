#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

import os
import string
from uuid import uuid4
from random import sample, randint

from zca import zca_manager as zca
from zzparameters import Parameters
from zzutils import BaseTest

from ...esearch import load_esearch_utilities
from ...esearch.interfaces import ICluster
from ...document import DirectoryDocumentTypeRegistry

from .. import load_esstore_utilities
from ..interfaces import IDocumentIndexRouter

class BaseTestCase(BaseTest):

    test_dir = os.path.dirname(__file__)

    def _gen_sample_doc(self): 
        return {
            'uuid': str(uuid4()),
            'foo': sample(string.lowercase, 8),
            'biz': randint(1, 100),
        }

    def _cleanup_index(self, index_name=None, cluster_name="default"):
        index_name = index_name or self.sample_index
        if 'unittest' not in index_name:
            raise ValueError("refuse delete non unittest index %s" % index_name)
        c = zca.get_utility(ICluster, cluster_name)
        c.delete_index(index_name)
    
    def _load_params(self):

        self.params = Parameters()
        GP = self.params.get_parameter

        d = os.path.join(self.test_dir, "doc_types")
        zca.register_utility(DirectoryDocumentTypeRegistry(d))

        fname = os.path.join(self.test_dir, "config.yml")
        self.params.load_param_file(fname)

        c_cfg = GP("elasticsearch.clusters")
        load_esearch_utilities(c_cfg)

        dir_cfg = GP("esstore.document_index_route")
        load_esstore_utilities(c_cfg)

        self.di_router = zca.get_utility(IDocumentIndexRouter)

