#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

import os
from nose.tools import *
from .base import BaseTestCase

from ..store import ESStore

class TestCluster(BaseTestCase):

    doc_type = "test_foo"

    doc_id = "adfs"
    doc = { 'foo_id': doc_id, 'biz': 10, }

    def setUp(self):
        self._load_params()
        self.add_cleanup(self._cleanup_index, ["unittest_fooness", ])

    def test_route(self):

        idx = self.di_router.route("test")
        eq_(idx.index_read_name, "test")

    def test_dt_route(self):

        idx = self.di_router.route("test_foo")
        eq_(idx.index_read_name, "unittest_fooness")

    def test_dt_route_override(self):

        idx = self.di_router.route("test_foo", "snargle")
        eq_(idx.index_read_name, "snargle")

    def test_es_list(self):

        s = ESStore("default")
        eq_(list(s.list(self.doc_type)), [])

    def test_get_put(self):

        s = ESStore("default")
        s.put(self.doc_type, self.doc_id, self.doc)
        s.refresh(self.doc_type)
        eq_(list(s.list(self.doc_type)), [self.doc_id])

        d = s.get(self.doc_type, self.doc_id)
        eq_(d, self.doc)

    def test_delete_count(self):

        s = ESStore("default")
        s.put(self.doc_type, self.doc_id, self.doc)
        s.refresh(self.doc_type)
        eq_(s.count(self.doc_type), 1)

        s.delete(self.doc_type, self.doc_id)
        s.refresh(self.doc_type)
        eq_(s.count(self.doc_type), 0)

    def test_initialize(self):
        
        s = ESStore("default")
        s.initialize(self.doc_type, {})




