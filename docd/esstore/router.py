#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from __future__ import absolute_import

import logging; log = logging.getLogger("esearch")
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error

#from zope.interface.interfaces import ComponentLookupError

from zca import zca_manager as zca
from .interfaces import IDocumentIndexRouter

from ..document.interfaces import IDocumentTypeRegistry
from ..esearch.interfaces import IIndexRouter

@zca.implementer(IDocumentIndexRouter)
class DocumentIndexRouter(object):

    def __init__(self, document_index_registry=None):

        self.document_index_registry = document_index_registry or {}

    def _get_index_name(self, doc_type, index_name):

        if index_name: 
            return index_name

        dt_iname = doc_type.meta.get("elasticsearch", {}).get('index_name')
        if dt_iname:
            return dt_iname

        r = self.document_index_registry
        return r.get(doc_type.name, doc_type.name)

    def route(self, doc_type_name, index_name=None, cluster_name=None):

        """route: given an document_type and possibly an index_name
            and cluster_name, return an index
        """

        dtr = zca.get_utility(IDocumentTypeRegistry)
        dt = dtr.new(doc_type_name)

        index_name = self._get_index_name(dt, index_name)
        dt_istyle = dt.meta.get("elasticsearch", {}).get('index_style')
        ir = zca.get_utility(IIndexRouter)

        return ir.route(index_name, cluster_name, style=dt_istyle)

# vi: ts=4 expandtab


