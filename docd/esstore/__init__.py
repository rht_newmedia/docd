
from zca import zca_manager as zca

from .store import ESStore
from .router import DocumentIndexRouter

def load_esstore_utilities(document_index_map=None):

    dmap = document_index_map or {}
    zca.register_utility(DocumentIndexRouter(dmap))


