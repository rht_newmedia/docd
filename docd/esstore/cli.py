#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
logging.basicConfig(level=logging.INFO)

import os
import click

from zca import zca_manager as zca
from zzutils import as_yaml

from .interfaces import ICluster

@click.group()
@click.pass_context
def esearch(ctx):
    pass

@esearch.group()
@click.option("-n", "--name", help="cluster name")
@click.pass_context
def cluster(ctx, name):
    ctx.cluster = zca.get_utility(ICluster, name)

@cluster.command()
@click.pass_context
def info(ctx):

    c = ctx.parent.cluster
    click.echo(as_yaml(c.info))

# vi: ts=4 expandtab
