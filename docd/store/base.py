#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

from contextlib import contextmanager
import os

import yaml

from zzutils import indir
from zca import zca_manager as zca
from .interfaces import IStore

@zca.implementer(IStore)
class BaseStore(object):

    path = ""
    storage_type = ""

    STORE_REGISTRY = {}

    @classmethod
    def register_store(cls, kls, s_type=""):

        s_type = s_type or getattr(kls, "storage_type", None)

        if not s_type:
            raise KeyError("could not determine storage type")

        if s_type in cls.STORE_REGISTRY:
            raise KeyError("duplicate key in store register: %s" % s_type)

        cls.STORE_REGISTRY[s_type] = kls
        return kls

    @classmethod
    def new_store(cls, s_type, *args, **kw):
        return cls.STORE_REGISTRY[s_type](*args, **kw)

    def initialize(self, doc_type_name, mata): pass

    def get(self, doc_type, doc_id): raise NotImplemented
    def put(self, doc_type, doc_id, doc): raise NotImplemented
    def list(self, doc_type): raise NotImplemented
    def delete(self, doc_type, doc_id): raise NotImplemented

    def count(self, doc_type):
        return len(self.list(doc_type))

@BaseStore.register_store
class MemoryStore(BaseStore):

    storage_type = "memory"

    def __init__(self, path="(memory)"): 
        self.path = path
        self.storage = {}

    def get(self, doc_type, doc_id): 
        return self.storage.get(doc_type, {})[doc_id]

    def put(self, doc_type, doc_id, doc):
        self.storage.setdefault(doc_type, {})[doc_id] = doc

    def list(self, doc_type):
        return self.storage.get(doc_type, {}).keys()

    def delete(self, doc_type, doc_id):
        self.storage.get(doc_type, {}).pop(doc_id, None)

# vi: ts=4 expandtab


