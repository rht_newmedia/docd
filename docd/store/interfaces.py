#!/usr/bin/python
#
#  Copyright (c) 2014 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

from zope.interface import Interface, Attribute

class IStore(Interface):

    """Store: persistant storage for documens
    """

    storage_type = Attribute("Storage type")
    path = Attribute("Storage type specific identifier for store")

    def get(doc_type, doc_id):

        """ return instance of doc type """

    def put(doc_type, doc_id, doc):

        """ store instance of doc type """

    def count(doc_type):
        """ return number of doc type documents """
        
    def list(doc_type):
        
        """ return ids of doc type """

    def delete(doc_type, doc_id):

        """ remove identified document """

#    def scan(doc_type, **kw):
#        
#        """ return iterator for all hits of given index and doc_type """
#
#    def bulk(doc_type, doc_src_stream, **kw):
#        
#        """ return iterator for all hits of given index and doc_type """


# vi: ts=4 expandtab


