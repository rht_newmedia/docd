#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
logging.basicConfig(level=logging.INFO)

import os
import click

from zzutils import as_yaml
from zca import zca_manager as zca

from ..impexp import ImportExportFormat
from ..document import DocumentStore
from ..document.interfaces import IDocumentTypeRegistry

from .interfaces import IStore

@click.group()
@click.option("-n", "--name", help="store name")
@click.option("-d", "--doc_type", help="document type")
@click.pass_context
def store(ctx, name, doc_type):

    store = zca.get_utility(IStore, name=name)
    dt = zca.get_utility(IDocumentTypeRegistry).new(doc_type)
    ctx.doc_store = DocumentStore(dt, store)

@store.command()
@click.pass_context
def count(ctx):
    click.echo(ctx.parent.doc_store.count())

@store.command()
@click.pass_context
def list(ctx):
    click.echo(os.linesep.join(ctx.parent.doc_store.list()))

@store.command()
@click.option("-f", "--format", help="format", default="json_line")
@click.option("-n", "--name", help="query name", default="")
@click.argument("output", type=click.File("w"), default="-")
@click.pass_context
def query(ctx, format, name, output):
    d = ctx.parent.doc_store
    f = ImportExportFormat.new(format)
    res = d.query(name)
    if not name:
        click.echo("queries:\n  %s" % "\n  ".join(res))
        return

    f.write_docs(d.query(name), output)

@store.command()
@click.option("-f", "--format", help="format", default="json_line")
@click.argument("output", type=click.File("w"), default="-")
@click.pass_context
def scan(ctx, format, output):
    d = ctx.parent.doc_store
    f = ImportExportFormat.new(format)
    f.write_docs(d.scan(), output)

@store.command()
@click.argument("doc_id")
@click.pass_context
def get(ctx, doc_id):
    doc = ctx.parent.doc_store.get(doc_id)
    click.echo(as_yaml(doc))

@store.command()
@click.argument("doc_id")
@click.pass_context
def delete(ctx, doc_id):
    ctx.parent.doc_store.delete(doc_id)

@store.command()
@click.argument("input", type=click.File())
@click.pass_context
def put(ctx, input):
    raise NotImplementedError

@store.command()
@click.option("-f", "--format", help="stream format", default="json_line")
@click.argument("input", type=click.File())
@click.pass_context
def put_objects(ctx, format, input):
    f = ImportExportFormat.new(format)
    ctx.parent.doc_store.bulk_insert(f.doc_iter_from_stream(input))

@store.command()
@click.pass_context
def init(ctx):
    click.echo(ctx.parent.doc_store.initialize())

# vi: ts=4 expandtab
