#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

from contextlib import contextmanager
import os

import yaml

from zzutils import indir
from zca import zca_manager as zca

from .interfaces import IStore
from .base import BaseStore

@zca.implementer(IStore)
@BaseStore.register_store
class FilesystemStore(BaseStore):

    storage_type = "filesystem"
    extension = ".yml"

    def __init__(self, path):
        self.path = path

    def _is_valid_doc_id(self, doc_id):
        return not ( "/" in doc_id or doc_id.startswith("_") )

    def _is_valid_filename(self, filename):
        return filename.endswith(self.extension)

    def _doc_id_to_filename(self, doc_id):
        if self._is_valid_doc_id(doc_id):
            return "%s%s" % (doc_id, self.extension)

    def _filename_to_doc_id(self, filename):
        f = filename.replace(self.extension, "")
        return f if self._is_valid_doc_id(f) else None

    @contextmanager
    def in_store(self, create=True):
        with indir(self.path, create):
            yield

    @contextmanager
    def in_doc_type(self, doc_type, create=True):
        with self.in_store(create):
            with indir(doc_type, create):
                yield

    def get(self, doc_type, doc_id):

        with self.in_doc_type(doc_type, create=False):
            fname = self._doc_id_to_filename(doc_id)
            if os.path.exists(fname):
                with open(fname) as f:
                    return yaml.load(f.read())

    def put(self, doc_type, doc_id, doc):

        with self.in_doc_type(doc_type):
            fname = self._doc_id_to_filename(doc_id)
            txt = yaml.dump(doc, default_flow_style=False)
            txt = txt.replace("!!python/unicode ", "")
            with open(fname, "w") as f:
                f.write(txt)

    def list(self, doc_type):

        try:
            with self.in_doc_type(doc_type, create=False):
                ff = filter(self._is_valid_filename, os.listdir("."))
                return filter(None, map(self._filename_to_doc_id, ff))
        except OSError, e:
            if e.errno == 2: # ENOENT
                return []
            raise                       # pragma: nocover

    def delete(self, doc_type, doc_id):

        with self.in_doc_type(doc_type, create=False):
            fname = self._doc_id_to_filename(doc_id)
            if os.path.exists(fname):
                os.unlink(fname)

# vi: ts=4 expandtab


