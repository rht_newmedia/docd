
from base import BaseStore
from filesystem import FilesystemStore
from zca import zca_manager as zca

def load_store_configuration(storage_config):

    for name, config in storage_config.items():
        s_type = config.pop("type")
        s = BaseStore.new_store(s_type, **config)
        if s:
            zca.register_utility(s, name=name)

