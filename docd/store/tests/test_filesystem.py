#!/usr/bin/env python
#
#  Copyright (c) 2015 Red Hat, Inc.  <bowe@redhat.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
"""
"""
__author__  = 'Bowe Strickland <bowe@redhat.com>'
__docformat__ = 'restructuredtext'

import logging; log = logging.getLogger(__name__)
DEBUG=log.debug; INFO=log.info; WARN=log.warning; ERROR=log.error
#logging.basicConfig(level=logging.INFO)

from tempfile import mkdtemp
from shutil import rmtree

from nose.tools import *
from zzutils import BaseTest

from ..filesystem import FilesystemStore

from uuid import uuid4


class FilesystemStoreTest(BaseTest):

    doc_type = "testdoc"

    test_id = str(uuid4())
    test_doc = {
        'uuid': test_id,
        'name': "foo",
        'value': 3,
    }

    def setUp(self):
        
        self.basedir = mkdtemp()
        self.store = FilesystemStore(self.basedir)

    def tearDown(self):

        rmtree(self.basedir)

    def test_empty_list(self):

        eq_(self.store.list(self.doc_type), [])

    def test_put(self):

        self.store.put(self.doc_type, self.test_id, self.test_doc)
        oo = self.store.list(self.doc_type)
        eq_(len(oo), 1)
        eq_(oo[0], self.test_id)

    def test_get(self):

        self.store.put(self.doc_type, self.test_id, self.test_doc)
        d = self.store.get(self.doc_type, self.test_id)
        eq_(d, self.test_doc)

    def test_delete(self):

        self.store.put(self.doc_type, self.test_id, self.test_doc)
        eq_(self.store.list(self.doc_type),  [self.test_id,])
        self.store.delete(self.doc_type, self.test_id)
        eq_(self.store.list(self.doc_type), [])

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)
    import sys

# vi: ts=4 expandtab


